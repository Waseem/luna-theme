#!/usr/bin/env bash

echo 'Updating apt-get registry and clean up ...' | wall

apt-get update
apt-get autoremove

echo 'Starting installation' | wall

# install packages
apt-get -q -y --force-yes install git nginx nodejs npm

rm -rf /var/www
ln -fs /vagrant/html /var/www
sudo cp /vagrant/docs/nginx /etc/nginx/sites-enabled/default
sudo /etc/init.d/nginx restart
sudo npm install -g bower
sudo npm install -g less
sudo ln -s /usr/bin/nodejs /usr/bin/node
cd /vagrant
bower install --allow-root
